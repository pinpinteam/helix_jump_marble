﻿using UnityEngine;
using UnityEditor;
using System.IO;
using UnityEngine.Rendering;

namespace Pinpin.Editor
{
	[CustomEditor(typeof(SquareSpawner))]
	public class SquareSpawnerEditor : UnityEditor.Editor
	{
		public override void OnInspectorGUI ()
		{
			base.OnInspectorGUI();

			SquareSpawner s = (SquareSpawner)target;

			if (GUILayout.Button("Spawn Prefabs"))
			{
				s.Spawn();
				EditorUtility.SetDirty(s.gameObject);
			}
		}
	}
}