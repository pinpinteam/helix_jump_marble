using UnityEngine;
using UnityEngine.Audio;
using System;

namespace Pinpin
{
	[CreateAssetMenu(fileName = "GameAssets", menuName = "Game/GameAssets", order = 1)]
	public class GameAssets: ScriptableObject
	{
		public AudioMixer	audioMixer;

		public Level[] levels;
		public GeneratedLevel generatedLevel;

		[Header("Pie Chart Element")]
		public PieChartElement[] easyPieChartElement;
		public PieChartElement[] mediumPieChartElement;
		public PieChartElement[] hardPieChartElement;
		public PieChartElement[] FirstPieChartElement;
		public PieChartElement[] BadPieChartElement;
		[Space]

		public Finisher[] finisher;

		public ColorTheme[] colorThemes;

		public Ball ballPrefab;
		public MeshRenderer ballGroundImpactPrefab;

	}

	[System.Serializable]
	public class ColorTheme
	{
		public String name;
		public Material skybox;
		public Material ballMaterial;
		public Material[] pieChartPartColorMaterials;
		public Material rotationCylinder;
	}
}