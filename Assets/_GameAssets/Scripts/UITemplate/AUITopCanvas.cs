﻿using System;
using System.Collections;
using UnityEngine;

namespace Pinpin.UI
{
	[DisallowMultipleComponent,
	DefaultExecutionOrder(-45)]
	public class AUITopCanvas : MonoBehaviour
	{
		protected AUIManager UIManager { get; private set; }

		protected bool m_visible;
		public virtual bool Visible
		{
			get
			{
				return m_visible;
			}
			set
			{
				m_visible = value;
			}
		}

		public void SetUIManager(AUIManager manager)
		{
			this.UIManager = manager;
		}

		public new virtual Type GetType()
		{
			return (base.GetType());
		}
	}
}
