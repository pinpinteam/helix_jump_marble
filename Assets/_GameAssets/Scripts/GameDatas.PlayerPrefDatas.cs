﻿using UnityEngine;
using Pinpin.Types;

namespace Pinpin
{

	public partial class GameDatas: ScriptableObject
	{

		[System.Serializable]
		public class PlayerPrefDatas
		{
			
			public bool			soundActive = true;
			public bool			vibrationActive = true;
			public float		sfxVolume = 1f;
			public float		musicVolume = 1f;
			public bool			enableOENotifications = false;
			public bool			firstTimeOEPopup = true;

			public static PlayerPrefDatas LoadDatas ()
			{
				PlayerPrefDatas datas = new PlayerPrefDatas();
				
				if (PlayerPrefs.HasKey(PlayerPrefKey.SoundActive))
					datas.soundActive = PlayerPrefs.GetInt(PlayerPrefKey.SoundActive) != 0;

				if (PlayerPrefs.HasKey(PlayerPrefKey.VibrationActive))
					datas.vibrationActive = PlayerPrefs.GetInt(PlayerPrefKey.VibrationActive) != 0;

				if (PlayerPrefs.HasKey(PlayerPrefKey.MusicVolume))
					datas.musicVolume = PlayerPrefs.GetFloat(PlayerPrefKey.MusicVolume);

				if (PlayerPrefs.HasKey(PlayerPrefKey.SfxVolume))
					datas.sfxVolume = PlayerPrefs.GetFloat(PlayerPrefKey.SfxVolume);

				if (PlayerPrefs.HasKey(PlayerPrefKey.EnableOENotifications))
					datas.enableOENotifications = PlayerPrefs.GetInt(PlayerPrefKey.EnableOENotifications) != 0;

				if (PlayerPrefs.HasKey(PlayerPrefKey.FirstTimeOEPopup))
					datas.firstTimeOEPopup = PlayerPrefs.GetInt(PlayerPrefKey.FirstTimeOEPopup) != 0;
				

				return (datas);
			}

			public void SaveDatas ()
			{
				PlayerPrefs.SetInt(PlayerPrefKey.SoundActive, this.soundActive ? 1 : 0);
				PlayerPrefs.SetInt(PlayerPrefKey.VibrationActive, (this.vibrationActive ? 1 : 0));
				PlayerPrefs.SetFloat(PlayerPrefKey.MusicVolume, this.musicVolume);
				PlayerPrefs.SetFloat(PlayerPrefKey.SfxVolume, this.sfxVolume);
				PlayerPrefs.SetInt(PlayerPrefKey.FirstTimeOEPopup, this.firstTimeOEPopup ? 1 : 0);
			}

		}
	
	}

}