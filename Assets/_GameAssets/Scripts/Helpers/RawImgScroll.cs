﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RawImgScroll : MonoBehaviour
{
	[SerializeField]
	private RawImage _img;

	public float scroolSpeedX = 2f;
	public float scroolSpeedY = 2f;

	// Update is called once per frame
	void Update()
	{
		_img.uvRect = new Rect(Time.unscaledTime * scroolSpeedX, Time.unscaledTime * scroolSpeedY, _img.uvRect.width, _img.uvRect.height);
	}
}
