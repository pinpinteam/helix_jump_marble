﻿using System;

namespace Pinpin.Utils
{
	public class ListenableProperty<T> where T : IEquatable<T>
	{
		private Action<T> m_onChanged;
		private T m_value;


		public T value
		{
			get { return m_value; }
			set
			{
				T last;

				last = m_value;
				m_value = value;
				if (!last.Equals(value) && m_onChanged != null)
					m_onChanged.Invoke(m_value);
			}
		}

		public ListenableProperty(T value, Action<T> callback)
		{
			this.m_value = value;
			this.m_onChanged = callback;

		}

		public void AddListener(Action<T> callback)
		{
			m_onChanged += callback;
		}

		public void RemoveListener(Action<T> callback)
		{
			m_onChanged -= callback;
		}

		public void RemoveAllListeners()
		{
			m_onChanged = null;
		}


		public static implicit operator T(ListenableProperty<T> property)
		{
			return property.value;
		}

	}
}
