﻿using System.Globalization;
using UnityEngine;
using System.Collections.Generic;

namespace Pinpin.Helpers
{

	public static class MathHelper
	{

		public static float MapBetween ( float value, float valueMin, float valueMax, float resultMin, float resultMax )
		{
			return (resultMax - resultMin) / (valueMax - valueMin) * (value - valueMax) + resultMax;
		}

		private static string[] EndLetters = new string[] { "", "K", "M", "B", "T", "Qa", "Qi", "Sx", "Sp", "Oc", "No", "Dc", "Un", "Du" };

		public static string ConvertToEngineeringNotation ( ulong value, int significantNumbers = 6, int decimalCount = 3)
		{
			string rawValue = value.ToString();
			int lenght = rawValue.Length;
			if (lenght > significantNumbers)
			{
				int thousandMultiplier = (lenght - 1) / 3;
				int thousandNumbers = lenght % 3;

				if (thousandNumbers == 0)
					thousandNumbers = 3;
				string resultString;
				resultString = new string(rawValue.ToCharArray(0, thousandNumbers));
				if (significantNumbers - thousandNumbers > 0)
					resultString += "." + new string(rawValue.ToCharArray(thousandNumbers, Mathf.Min(significantNumbers - thousandNumbers, decimalCount)));
				float result = float.Parse(resultString, CultureInfo.InvariantCulture);
				return result.ToString(ApplicationManager.currentCulture.NumberFormat) + EndLetters[thousandMultiplier];
			}
			else
			{
				return int.Parse(rawValue).ToString(ApplicationManager.currentCulture.NumberFormat);
			}
		}


		public static ulong Fibonnaci (int lv)
		{
			if (lv == 0)
				return 1;
			ulong baseScore = 1;
			ulong score = 0;
			ulong temp = 0;
			for (int i = 0; i < lv; i++)
			{
				temp = score;
				score = baseScore;
				baseScore += temp;
			}
			return score;
		}

		public static List<Vector3> GetPositionListCircle ( Vector3 startPosition, float distanceStep, int positionCount )
		{
			List<Vector3> positionList = new List<Vector3>();

			var ringCount = 0;
			var positionIncrementation = 0;

			while (positionIncrementation < positionCount)
			{
				positionIncrementation += ringCount == 0 ? 1 : ringCount + (int)Mathf.Pow(2, ringCount + 1);
				ringCount++;
			}

			int positionCountInCircle;
			int positionRemaining = positionCount;

			//foreach circle
			for (int i = 0; i < ringCount; i++)
			{
				if (i == 0)
				{
					positionCountInCircle = 1;
					positionRemaining -= 1;
				}
				else if (i == ringCount - 1)
				{
					positionCountInCircle = positionRemaining;
				}
				else
				{
					positionCountInCircle = i + (int)Mathf.Pow(2, i + 1);
					positionRemaining -= positionCountInCircle;
				}

				positionList.AddRange(GetPositionListAround(startPosition, i * distanceStep, positionCountInCircle));
			}

			return positionList;
		}

		public static List<Vector3> GetPositionListAround ( Vector3 startPosition, float distance, int positionCount )
		{
			List<Vector3> positionList = new List<Vector3>();

			for (int i = 0; i < positionCount; i++)
			{
				float angle = i * (360f / positionCount);

				Vector3 direction = new Vector3(Mathf.Cos(angle * Mathf.Deg2Rad), 0, Mathf.Sin(angle * Mathf.Deg2Rad));
				Vector3 position = startPosition + direction * distance;
				positionList.Add(position);
			}

			return positionList;
		}

	}

}