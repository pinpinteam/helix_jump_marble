using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using DG.Tweening;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Pinpin.Helpers
{
	[RequireComponent(typeof(CinemachineBrain))]
	public class CinemachineScreenShakeHelper : MonoBehaviour
	{
		[SerializeField] private NoiseSettings m_noiseProfile;
		[SerializeField] private float m_masterGain = 1f;

		//[Header("Debug")]
		//[SerializeField] private float m_debugGain = 1;
		//[SerializeField] private float m_debugDuration = 1;
		//[SerializeField] private float m_debugFrequency = 1;

		private CinemachineBrain m_brain;
		private List<ShakeCamera> m_cameraSetupList = new List<ShakeCamera>();

		private CinemachineBasicMultiChannelPerlin m_currentMultiChannelPerlin;
		private CinemachineBasicMultiChannelPerlin CurrentMultiChannelPerlin
		{
			get => m_currentMultiChannelPerlin;
			set
			{
				float onGoingGain = 0;
				float onGoingFrequency = 1f;
				if (m_currentMultiChannelPerlin != null)
				{
					onGoingGain = m_currentMultiChannelPerlin.m_AmplitudeGain;
					onGoingFrequency = m_currentMultiChannelPerlin.m_FrequencyGain;
					if (m_currentShakeCoroutine != null)
					{
						m_currentMultiChannelPerlin.m_AmplitudeGain = 0;
						m_currentMultiChannelPerlin.m_FrequencyGain = 1;
					}
				}

				m_currentMultiChannelPerlin = value;

				if (value == null) return;
				m_currentMultiChannelPerlin.m_AmplitudeGain = onGoingGain;
				m_currentMultiChannelPerlin.m_FrequencyGain = onGoingFrequency;
			}
		}

		private Coroutine m_currentShakeCoroutine;
		private Tweener m_currentShakeTween;

#if UNITY_EDITOR
		private void Reset ()
		{
			m_noiseProfile = AssetDatabase.LoadAssetAtPath<NoiseSettings>("Packages/com.unity.cinemachine/Presets/Noise/6D Shake.asset");
		}
#endif

		private void Awake ()
		{
			m_brain = GetComponent<CinemachineBrain>();
			m_brain.m_CameraActivatedEvent.AddListener(OnVirtualCameraActivated);
		}

		public void OnVirtualCameraActivated ( ICinemachineCamera to, ICinemachineCamera from )
		{
			ShakeCamera shakeCamera = TryGetShakeCamera(to.VirtualCameraGameObject);
			if (shakeCamera != null)
			{
				CurrentMultiChannelPerlin = shakeCamera.multiChannelPerlin;
				return;
			}

			CinemachineVirtualCamera vTo = to.VirtualCameraGameObject.GetComponent<CinemachineVirtualCamera>();
			CurrentMultiChannelPerlin = vTo.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
			if (CurrentMultiChannelPerlin == null)
			{
				CurrentMultiChannelPerlin = vTo.AddCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
			}
			CurrentMultiChannelPerlin.m_NoiseProfile = m_noiseProfile;
			if(to.VirtualCameraGameObject.GetComponent<OnDestroyEvent>() == null)
				to.VirtualCameraGameObject.AddComponent<OnDestroyEvent>().onDestroy += OnVirtualCameraDestroyed;
			else
				to.VirtualCameraGameObject.GetComponent<OnDestroyEvent>().onDestroy += OnVirtualCameraDestroyed;
			m_cameraSetupList.Add(new ShakeCamera(to.VirtualCameraGameObject, CurrentMultiChannelPerlin));
		}

		private void OnVirtualCameraDestroyed ( GameObject destroyedObject )
		{
			m_cameraSetupList.RemoveAll(x => x.virtualCameraGameObject == destroyedObject);
		}

		private ShakeCamera TryGetShakeCamera ( GameObject virtualCameraObject )
		{
			return m_cameraSetupList.Find(x => x.virtualCameraGameObject == virtualCameraObject);
		}

		private void ClearCameraList ()
		{
			m_cameraSetupList.Clear();
		}

		//private void Update ()
		//{
		//	if (Input.GetKeyDown(KeyCode.S))
		//	{
		//		Shake(m_debugGain, m_debugDuration, m_debugFrequency);
		//	}
		//	if (Input.GetKeyDown(KeyCode.Alpha1))
		//	{
		//		Shake(ShakePreset.Light);
		//	}
		//	if (Input.GetKeyDown(KeyCode.Alpha2))
		//	{
		//		Shake(ShakePreset.Subtle);
		//	}
		//	if (Input.GetKeyDown(KeyCode.Alpha3))
		//	{
		//		Shake(ShakePreset.Hard);
		//	}
		//	if (Input.GetKeyDown(KeyCode.Alpha4))
		//	{
		//		Shake(ShakePreset.Explosion);
		//	}
		//}

		#region Shake Methods

		public void Shake ( float gain, float duration, float frequency = 1 )
		{
			float totalGain = gain * m_masterGain;
			if (totalGain < CurrentMultiChannelPerlin.m_AmplitudeGain)
				return;

			CancelShake();

			m_currentShakeCoroutine = StartCoroutine(ShakeCoroutine());

			IEnumerator ShakeCoroutine ()
			{
				CurrentMultiChannelPerlin.m_AmplitudeGain = totalGain;
				CurrentMultiChannelPerlin.m_FrequencyGain = frequency;
				yield return new WaitForSeconds(duration);
				CurrentMultiChannelPerlin.m_AmplitudeGain = 0;
				CurrentMultiChannelPerlin.m_FrequencyGain = 1;
				m_currentShakeCoroutine = null;
			}
		}

		public void Shake ( float gain, float duration, Ease ease, float frequency = 1 )
		{
			float totalGain = gain * m_masterGain;
			if (totalGain < CurrentMultiChannelPerlin.m_AmplitudeGain)
				return;

			CancelShake();

			CurrentMultiChannelPerlin.m_FrequencyGain = frequency;
			m_currentShakeTween = DOTween.To(() => totalGain, x => totalGain = x, 0, duration).SetEase(ease).OnUpdate(() =>
			{
				CurrentMultiChannelPerlin.m_AmplitudeGain = totalGain;
			}).OnComplete(() =>
			{
				CurrentMultiChannelPerlin.m_FrequencyGain = 1;
			});
		}

		public void Shake ( ShakePreset preset )
		{
			ReadPreset(preset, out float currentGain, out float currentDuration, out Ease currentEase, out float currentFrequency);

			if (currentEase == Ease.Unset)
			{
				Shake(currentGain, currentDuration, currentFrequency);
			}
			else
			{
				Shake(currentGain, currentDuration, currentEase, currentFrequency);
			}
		}

		public void Shake ( ShakePreset preset, float overrideGain )
		{
			ReadPreset(preset, out _, out float currentDuration, out Ease currentEase, out float currentFrequency);

			if (currentEase == Ease.Unset)
			{
				Shake(overrideGain, currentDuration, currentFrequency);
			}
			else
			{
				Shake(overrideGain, currentDuration, currentEase, currentFrequency);
			}
		}

		private void ReadPreset ( ShakePreset preset, out float outGain, out float outDuration, out Ease outEase, out float outFrequency )
		{
			outGain = 1;
			outDuration = 1;
			outEase = Ease.Unset;
			outFrequency = 1;
			switch (preset)
			{
				case ShakePreset.Light:
					outGain = 0.5f;
					outDuration = 0.5f;
					outEase = Ease.InSine;
					outFrequency = 1;
					break;
				case ShakePreset.Subtle:
					outGain = 1f;
					outDuration = 0.2f;
					outFrequency = 3;
					break;
				case ShakePreset.Hard:
					outGain = 3f;
					outDuration = 0.5f;
					outEase = Ease.InOutSine;
					outFrequency = 3f;
					break;
				case ShakePreset.Explosion:
					outGain = 5f;
					outDuration = 2f;
					outEase = Ease.OutSine;
					outFrequency = 0.05f;
					break;
			}
		}

		public void CancelShake ()
		{
			if (m_currentShakeCoroutine != null)
			{
				StopCoroutine(m_currentShakeCoroutine);
				m_currentShakeCoroutine = null;
			}

			m_currentShakeTween?.Pause();

			CurrentMultiChannelPerlin.m_AmplitudeGain = 0;
			CurrentMultiChannelPerlin.m_FrequencyGain = 1;
		}

		#endregion

		class ShakeCamera
		{
			public GameObject virtualCameraGameObject;
			public CinemachineBasicMultiChannelPerlin multiChannelPerlin;

			public ShakeCamera ( GameObject _virtualCameraGameObject, CinemachineBasicMultiChannelPerlin _multiChannelPerlin )
			{
				virtualCameraGameObject = _virtualCameraGameObject;
				multiChannelPerlin = _multiChannelPerlin;
			}
		}
	}

	public enum ShakePreset
	{
		/// <summary>
		/// Gain .5f, Duration 0.5f, Frequency 1, Ease InSine
		/// </summary>
		Light,
		/// <summary>
		/// Gain 1f, Duration 0.2f, Frequency 3
		/// </summary>
		Subtle,
		/// <summary>
		/// Gain 3f, Duration 0.5f, Frequency 3, Ease InOutSine
		/// </summary>
		Hard,
		/// <summary>
		/// Gain 5f, Duration 2f, Frequency 0.05f, Ease OutSine
		/// </summary>
		Explosion
	}
}

