﻿using UnityEngine;

namespace Pinpin
{
	[ExecuteInEditMode]
	public class Camera3DResizer : MonoBehaviour
	{

		public enum ResizeMode
		{
			None,
			Crop,
			Zoom
		}

		[SerializeField] private float targetFieldOfView = 60f;
		[SerializeField] private Vector2 targetAspectRatio = new Vector2(9, 16);
		[SerializeField] private ResizeMode largestResizeMode;
		[SerializeField] private ResizeMode narrowResizeMode;
		[SerializeField] private bool inAnimation;
		[SerializeField] private new Camera camera;
		private bool m_fOVChanged;
		
		private Vector2Int screenSize { get; set; }
		private int ScreenWidth
		{
			get
			{
#if UNITY_EDITOR
				return (int)UnityEditor.Handles.GetMainGameViewSize().x;
#else
				return Screen.width;
#endif
			}
		}
		private int ScreenHeight
		{
			get
			{
#if UNITY_EDITOR
				return (int)UnityEditor.Handles.GetMainGameViewSize().y;
#else
				return Screen.height;
#endif
			}
		}



		private void Awake ()
		{
			OnScreenSizeChanged();
		}

		private void OnScreenSizeChanged ()
		{
			m_fOVChanged = false;
			if (ScreenHeight == 0)
				return;

			float currentRatio = ScreenWidth / (float)ScreenHeight;
			float targetRatio = targetAspectRatio.x / targetAspectRatio.y;
			screenSize = new Vector2Int(ScreenWidth, ScreenHeight);

			DefaultSize();
			if (Mathf.Approximately(currentRatio, targetRatio))
				return;

			if (currentRatio > targetRatio)
				LargestResize(currentRatio, targetRatio);
			else
				NarrowResize(currentRatio, targetRatio);

		}

		private void NarrowResize ( float currentRatio, float targetRatio )
		{
			float delta = currentRatio / targetRatio;
			switch (narrowResizeMode)
			{

				case ResizeMode.Crop:
					float yOffset = (1f - delta) / 2f;
					camera.rect = new Rect(0, yOffset, 1, delta);
					break;

				case ResizeMode.Zoom:
					camera.fieldOfView = targetFieldOfView / delta;
					break;
			}
		}

		private void LargestResize ( float currentRatio, float targetRatio )
		{
			float delta = targetRatio / currentRatio;
			switch (largestResizeMode)
			{

				case ResizeMode.Crop:
					float xOffset = (1f - delta) / 2f;
					camera.rect = new Rect(xOffset, 0, delta, 1);
					break;

				case ResizeMode.Zoom:
					camera.fieldOfView = targetFieldOfView * delta;
					break;
			}
		}

		public void SetFOV ( float fieldOfView )
		{
			targetFieldOfView = fieldOfView;
			m_fOVChanged = true;
		}

		public float GetFOV ()
		{
			return targetFieldOfView;
		}

		private void DefaultSize ()
		{
			camera.rect = new Rect(0, 0, 1, 1);
			camera.fieldOfView = targetFieldOfView;
		}

		private void Reset ()
		{
			if (camera == null)
			{
				camera = GetComponent<UnityEngine.Camera>();
				targetFieldOfView = camera.fieldOfView;
			}
		}

		private void OnValidate ()
		{
			if (!ApplicationManager.IsGameObjectPartOfCurrentPrefabOrScene(gameObject))
				return;

			OnScreenSizeChanged();
		}

		private void OnDestroy ()
		{
			if (camera != null)
			{
				camera.fieldOfView = targetFieldOfView;
			}
		}

		private void Update ()
		{
			if (screenSize.x != ScreenWidth || screenSize.y != ScreenHeight || inAnimation || m_fOVChanged)
				OnScreenSizeChanged();
		}
	}
}