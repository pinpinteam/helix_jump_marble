﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin.UI
{

	[ExecuteInEditMode]
	public class UIGaugeRect : UIGaugeObject
	{
		[SerializeField] private float m_min;
		[SerializeField] private float m_max;

		public float min { get { return m_min; } set { m_min = value; } }
		public float max { get { return m_max; } set { m_max = value; } }

		private float m_minRatio;
		private float m_maxRatio { get { return m_ratio; } set { m_ratio = value; } }

		public override void Init()
		{
			base.Init();

			Vector2 anchorMin, anchorMax;

			m_min = m_compass.min;
			m_max = m_compass.max;

			anchorMin = m_rect.anchorMin;
			anchorMax = m_rect.anchorMax;

			m_rect.anchoredPosition = Vector2.zero;
			m_rect.anchoredPosition = Vector2.zero;

			anchorMin[1 - (int)m_compass.axis] = 0;
			anchorMax[1 - (int)m_compass.axis] = 1;
			m_rect.pivot = new Vector2(0.5f, 0.5f);
		}

		protected override void Update()
		{
			base.Update();

			if (m_compass != null && m_rect != null)
			{
				Vector2 anchorMin, anchorMax;
				float minRatio, maxRatio;

				anchorMin = m_rect.anchorMin;
				anchorMax = m_rect.anchorMax;

				minRatio = (min - m_compass.min) / m_compass.max;
				maxRatio = max / m_compass.max;

				m_minRatio = m_compass.direction == UIGauge.EDirection.Invert ? 1 - minRatio : minRatio;
				m_maxRatio = m_compass.direction == UIGauge.EDirection.Invert ? 1 - maxRatio : maxRatio;

				anchorMin[(int)m_compass.axis] = Mathf.Clamp01(m_minRatio);
				anchorMax[(int)m_compass.axis] = Mathf.Clamp01(m_maxRatio);

				m_rect.anchorMin = anchorMin;
				m_rect.anchorMax = anchorMax;
			}
		}
	}
}
