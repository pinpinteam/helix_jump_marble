using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoMoveGameObject : MonoBehaviour
{
    [SerializeField] private float m_delay = 0f;
    [SerializeField] private float m_pause = 0f;
    [SerializeField] private float m_duration = 0.5f;
    [SerializeField] private Ease m_ease = Ease.Linear;
    [SerializeField] private Transform m_target;
    [SerializeField] private Vector3[] m_localPoses;
    private float m_currentPause;

    private Tweener m_tween;

    private void OnEnable ()
    {
        m_currentPause = m_delay;
        StartTween();
    }

    private void OnDisable ()
    {
        m_tween.Pause();
    }

    void StartTween ()
    {
        m_tween.Pause();
        m_tween.Kill();
        m_target.localPosition = m_localPoses[0];
        m_tween = m_target.DOLocalMove(m_localPoses[1], m_duration).SetDelay(m_currentPause).SetEase(m_ease);
        m_currentPause = m_pause;
        m_tween.onComplete += StartTween;
    }
}
