﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Camera_GrayscaleDebug : MonoBehaviour
{
	private Material material;
	[SerializeField] [Range(0, 1)] private float m_threshold;
	[SerializeField] [Range(0, 1)] private float m_debug;

	private void Awake ()
	{
		material = new Material(Shader.Find("Hidden/GrayscaleDebugger"));
	}

	void Reset ()
	{
		material = new Material(Shader.Find("Hidden/GrayscaleDebugger"));
	}

	void OnRenderImage ( RenderTexture source, RenderTexture destination )
	{
		SetParameters();

		if (material == null)
			return;

		Graphics.Blit(source, destination, material);	
	}

	private void SetParameters ()
	{
		material.SetFloat("_Treshold", m_threshold);
		material.SetFloat("_Debug", m_debug);
	}
}
