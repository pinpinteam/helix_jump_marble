﻿using Cinemachine;
using Pinpin.Helpers;
using Pinpin.InputControllers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MoreMountains.NiceVibrations;

namespace Pinpin
{
	public class Level : MonoBehaviour
	{
		public static Action<ulong, ulong> onBallCountChangedAction;

		[SerializeField] protected Transform m_levelToRotate = null;
		[SerializeField] protected MeshRenderer m_cylinderToRotate = null;
		[SerializeField] protected Transform m_startBallPosition = null;
		[SerializeField] protected Transform m_ballsParent = null;
		[SerializeField] protected CinemachineVirtualCamera m_virtualCamGame = null;
		[SerializeField] protected List<PieChartElement> m_pieChartElements = new List<PieChartElement>();
		[SerializeField] protected Transform m_cameraTarget = null;

		protected Vector3 m_currentRot;
		protected List<Ball> m_balls = new List<Ball>();
		protected Ball mainBall;
		protected float m_ballCurrentSpeed;
		protected float m_vibrationTimer;
		[HideInInspector] public ulong currentScoreFinisher = 0;

		private List<Vector3> m_positionList;
		private int m_wantedBalls;
		private ulong m_realBallCollectedCount;

		public int totalBallsCount
		{
			get { return m_balls.Count; }
		}

		protected GameConfig.GameSettings gameConfig
		{
			get { return ApplicationManager.config.game; }
		}

		protected virtual void Awake ()
		{
			GameManager.OnLevelStart += StartGame;
			PieChartPart.OnBallTriggerPieChartAction += BallTriggerPieChart;
			m_cylinderToRotate.material = ApplicationManager.assets.colorThemes[GameManager.Instance.colorThemeIndex].rotationCylinder;
			RenderSettings.skybox = ApplicationManager.assets.colorThemes[GameManager.Instance.colorThemeIndex].skybox;
			m_levelToRotate.transform.localScale = new Vector3(m_levelToRotate.transform.localScale.x, m_pieChartElements.Count * 2f, m_levelToRotate.transform.localScale.z);
		}

		protected virtual void Start ()
		{
			InstantiateBall();
		}

		protected virtual void OnDestroy ()
		{
			GameManager.OnLevelStart -= StartGame;
			PieChartPart.OnBallTriggerPieChartAction -= BallTriggerPieChart;
		}

		protected virtual void FixedUpdate ()
		{
			if (GameManager.Instance.gameState == GameManager.GameState.Finisher) 
				return;
			else if (GameManager.Instance.gameState == GameManager.GameState.InGame || GameManager.Instance.gameState == GameManager.GameState.InMenu)
			{
				RotateLevel();
				if(mainBall == null)
				{
					int newMainBallIndex = 0;
					for (int i = 1; i < totalBallsCount; i++)
					{
						if (m_balls[newMainBallIndex].transform.position.y > m_balls[i].transform.position.y)
						{
							newMainBallIndex = i;
						}
					}
					m_balls[newMainBallIndex].isMainBall = true;
					mainBall = m_balls[newMainBallIndex];
				}
				m_cameraTarget.transform.position = new Vector3(m_startBallPosition.position.x, mainBall.transform.position.y, m_startBallPosition.position.z);
			}
		}

		protected virtual void Update ()
		{
			if(GameManager.Instance.gameState != GameManager.GameState.EndGame)
				CollectibleVibration();
		}

		protected virtual void StartGame ()
		{

		}

		#region Vibration
		protected void CollectibleVibration ()
		{
			if (m_vibrationTimer > 0)
			{
				m_vibrationTimer -= Time.deltaTime;
			}
		}

		public void LightVibration ()
		{
			if (m_vibrationTimer <= 0)
			{
				m_vibrationTimer = gameConfig.lightHapticCooldown;
				MMVibrationManager.Haptic(HapticTypes.LightImpact);
			}
		}
		#endregion

		private void RotateLevel ()
		{
			if (InputManager.isTouching)
			{
				m_currentRot = Vector3.up * -InputManager.moveDelta.x * gameConfig.speedRotationLevel * Time.fixedDeltaTime;
				m_levelToRotate.Rotate(m_currentRot, Space.World);
			}
		}

		private void InstantiateBall ()
		{
			Ball ball = null;
			if (totalBallsCount == 0)
			{
				ball = Instantiate(ApplicationManager.assets.ballPrefab, m_startBallPosition.position, Quaternion.identity, m_ballsParent);
				ball.isMainBall = true;
				mainBall = ball;
				m_balls.Add(ball);
			}
			else if (totalBallsCount < 50)
			{
				Vector3 offset = GetBallOffSetVector(totalBallsCount);
				Vector3 ballPosition = new Vector3(m_balls[0].transform.position.x, mainBall.transform.position.y, m_balls[0].transform.position.z) + offset;

				ball = Instantiate(ApplicationManager.assets.ballPrefab, ballPosition, Quaternion.identity, m_ballsParent);

				if (ball.transform.position.y < mainBall.transform.position.y)
				{
					mainBall.isMainBall = false;
					ball.isMainBall = true;
					mainBall = ball;
				}
				ball.currentSpeed = mainBall.currentSpeed;

				m_balls.Add(ball);
			}
			m_realBallCollectedCount++;
			onBallCountChangedAction?.Invoke(m_realBallCollectedCount - 1, m_realBallCollectedCount);
		}

		private Vector3 GetBallOffSetVector ( int characterIndex )
		{
			//SPAWN In Circle
			if (m_positionList == null || m_positionList.Count != m_wantedBalls)
				m_positionList = MathHelper.GetPositionListCircle(m_balls[0].transform.position, gameConfig.ballOffset, m_wantedBalls);

			// need full random pos y
			Vector3 newPos = new Vector3(m_positionList[characterIndex].x - m_balls[0].transform.position.x, 0f, m_positionList[characterIndex].z - m_balls[0].transform.position.z); 

			Vector3 randomPos = UnityEngine.Random.insideUnitSphere * gameConfig.ballRandomOffSet;
			randomPos.y *= 20f;

			newPos += randomPos;

			return newPos;
		}

		private void RemoveBall ()
		{
			int ballIndex = m_balls.Count - 1;

			if (ballIndex < 0) return;
			else if (ballIndex == 0)
				GameManager.Instance.LevelFailed();

			if (m_balls[ballIndex].isMainBall && ballIndex != 0)
			{
				if(ballIndex == 1)
				{
					mainBall.isMainBall = false;
					m_balls[0].isMainBall = true;
					mainBall = m_balls[0];
				}
				else
					ballIndex--;
			}

			m_balls[ballIndex].AnimationDispawnBall();
			m_balls.RemoveAt(ballIndex);
			m_realBallCollectedCount--;
			onBallCountChangedAction?.Invoke(m_realBallCollectedCount + 1, m_realBallCollectedCount);
		}

		public void RemovePreciseBall (Ball ballToRemove)
		{
			if(totalBallsCount == 1)
				GameManager.Instance.LevelFailed();
			m_balls.Remove(ballToRemove);
			if (ballToRemove.isMainBall && totalBallsCount > 1)
			{
				mainBall.isMainBall = false;
				int newMainBallIndex = 0;
				for (int i = 1; i < totalBallsCount; i++)
				{
					if(m_balls[newMainBallIndex].transform.position.y > m_balls[i].transform.position.y)
					{
						newMainBallIndex = i;
					}
				}
				m_balls[newMainBallIndex].isMainBall = true;
				mainBall = m_balls[newMainBallIndex];
			}
			ballToRemove.AnimationDispawnBall();

			m_realBallCollectedCount--;
			onBallCountChangedAction?.Invoke(m_realBallCollectedCount + 1, m_realBallCollectedCount);
		}

		public void NewMainBall (Ball ball)
		{
			mainBall.isMainBall = false;
			mainBall = ball;
			ball.isMainBall = true;
		}

		private void BallTriggerPieChart ( PieChartPart.PieChartPartType pieChartPartType, int count )
		{
			switch (pieChartPartType)
			{
				case PieChartPart.PieChartPartType.Addition:
					m_wantedBalls = totalBallsCount + count;
					for (int i = 0; i < count; i++)
					{
						InstantiateBall();
					}
					break;
				case PieChartPart.PieChartPartType.Multiplication:
					int AdditionalBallCount = (totalBallsCount * count) - totalBallsCount;
					m_wantedBalls = totalBallsCount + AdditionalBallCount;
					for (int i = 0; i < AdditionalBallCount; i++)
					{
						InstantiateBall();
					}
					break;
				case PieChartPart.PieChartPartType.Soustraction:
					m_wantedBalls = totalBallsCount - count;
					for (int i = 0; i < count; i++)
					{
						RemoveBall();
					}
					break;
				case PieChartPart.PieChartPartType.Divison:
					int substractBallCount = totalBallsCount - (totalBallsCount /count);
					m_wantedBalls = totalBallsCount - substractBallCount;
					if (totalBallsCount - substractBallCount == 0 && totalBallsCount != 1)
					{
						substractBallCount -= 1;
					}
					for (int i = 0; i < substractBallCount; i++)
					{
						RemoveBall();
					}
					break;
			}
		}

		public void SlowMowEatchBalls ()
		{
			foreach (Ball ball in m_balls)
			{
				ball.currentSpeed = 0;
			}
		}

		#region Finisher
		public IEnumerator MoveBallFinisher (Transform[] position)
		{
			foreach(Ball ball in m_balls)
			{
				ball.transform.position = position[UnityEngine.Random.Range(0, position.Length)].position;
				ball.SetGravityFinisher(true);
				yield return new WaitForSeconds(gameConfig.durationBetweenBallMovePachinko);
			}
		}

		#endregion
	}
}
