﻿using UnityEngine;
using MoreMountains.NiceVibrations;

namespace Pinpin
{
	public class PieChartElement : MonoBehaviour
	{
		[SerializeField] private PieChartPart[] m_pieChartParts;

		private void Awake ()
		{
			foreach (PieChartPart pieChartPart in m_pieChartParts)
				pieChartPart.pieChartElement = this;
		}

		private void OnDestroy ()
		{
		}

		public void AnimationPieChartExplosion ()
		{
			MMVibrationManager.Haptic(HapticTypes.MediumImpact);
			transform.parent = transform.parent.parent;
			foreach (PieChartPart piechartPart in m_pieChartParts)
			{
				piechartPart.rigidbody.useGravity = true;
				piechartPart.meshcollider.enabled = false;
				piechartPart.rigidbody.AddExplosionForce(300f, new Vector3(piechartPart.canvasPosition.position.x * -1f, piechartPart.transform.position.y - 1f, piechartPart.canvasPosition.position.z * -1f), 10f);
				piechartPart.rigidbody.AddTorque(new Vector3(Random.Range(0f, 200f), Random.Range(-50f, 50f), Random.Range(-50f, 50f)));
			}
			Destroy(gameObject, 2f);
		}
	}
}