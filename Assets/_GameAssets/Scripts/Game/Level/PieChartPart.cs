﻿using System;
using TMPro;
using UnityEngine;

namespace Pinpin
{
	public class PieChartPart : MonoBehaviour
	{
		public static Action<PieChartPartType, int> OnBallTriggerPieChartAction;

		public PieChartPartType pieChartPartType;
		public Rigidbody rigidbody;
		[HideInInspector] public PieChartElement pieChartElement;

		public enum PieChartPartType
		{
			Basic,
			Multiplication,
			Divison,
			Addition,
			Soustraction,
			Empty,
		}
		[SerializeField] private TextMeshProUGUI m_textMeshPro = null;
		[SerializeField][Range(1, 10)] private int m_counterUpgrade = 2;
		[SerializeField] private MeshRenderer m_meshRenderer = null;
		[HideInInspector] public MeshCollider meshcollider;

		[Header("Color Offset")]
		[SerializeField] private float m_startValueColorOpacity = 1f;
		[SerializeField] private float m_lastValueColorOpacity = 0.3f;

		private int m_currentCounterUpgrade;

		public Transform canvasPosition;

		private bool m_isInteractible = true;

		private void Awake ()
		{
			meshcollider = m_meshRenderer.gameObject.AddComponent<MeshCollider>();
			meshcollider.convex = true;
			meshcollider.isTrigger = true;
			if (pieChartPartType == PieChartPartType.Empty)
				m_meshRenderer.enabled = false;

			InitPieChartPart();
		}

		private void OnDrawGizmos ()
		{
			if (pieChartPartType == PieChartPartType.Basic)
				Gizmos.color = Color.blue;
			else if (pieChartPartType == PieChartPartType.Addition || pieChartPartType == PieChartPartType.Multiplication)
				Gizmos.color = Color.green;
			else
				Gizmos.color = Color.red;
			Gizmos.DrawSphere(new Vector3(canvasPosition.position.x * -1f, transform.position.y - 1f, canvasPosition.position.z * -1f), 0.1f);
		}

		private void OnTriggerEnter ( Collider other )
		{
			if (other.CompareTag("Ball") && m_isInteractible)
			{
				Ball ball = other.GetComponent<Ball>();
				if(pieChartPartType == PieChartPartType.Soustraction)
				{
					m_currentCounterUpgrade--;
					ChangeColorOpacity(((float)m_currentCounterUpgrade / (float)m_counterUpgrade) * m_startValueColorOpacity);
					m_textMeshPro.text = "-" + m_currentCounterUpgrade.ToString();

					FXManager.Instance.PlayFxGlassBreak(new Vector3(other.transform.position.x, canvasPosition.position.y, other.transform.position.z));

					GameManager.Instance.currentLevel.RemovePreciseBall(ball);
					if(m_currentCounterUpgrade == 0)
					{
						pieChartElement.AnimationPieChartExplosion();
						canvasPosition.gameObject.SetActive(false);
					}
					GameManager.Instance.currentLevel.LightVibration();
					// remove ball
				}
				else if(pieChartPartType != PieChartPartType.Basic)
				{
					m_isInteractible = false;
					OnBallTriggerPieChartAction?.Invoke(pieChartPartType, m_counterUpgrade);
					pieChartElement.AnimationPieChartExplosion();
					GameManager.Instance.currentLevel.NewMainBall(ball);
				}
				else
				{
					Vector3 posImpact = new Vector3(other.transform.position.x, canvasPosition.position.y, other.transform.position.z);

					MeshRenderer ballGroundImpact = Instantiate(ApplicationManager.assets.ballGroundImpactPrefab, posImpact, ApplicationManager.assets.ballGroundImpactPrefab.transform.rotation);
					ballGroundImpact.material.SetColor("_Color", ApplicationManager.assets.colorThemes[GameManager.Instance.colorThemeIndex].ballMaterial.GetColor("_Color"));
					ballGroundImpact.transform.eulerAngles = new Vector3(ballGroundImpact.transform.eulerAngles.x, ballGroundImpact.transform.eulerAngles.y, UnityEngine.Random.Range(0f, 360f));
					ballGroundImpact.transform.localScale *= UnityEngine.Random.Range(0.8f, 1.2f);
					ballGroundImpact.transform.parent = transform;
					Destroy(ballGroundImpact.gameObject, 6f);

					GameManager.Instance.currentLevel.LightVibration();
				}
			}
		}

		private void InitPieChartPart ()
		{
			switch (pieChartPartType)
			{
				case PieChartPartType.Basic:
					m_textMeshPro.text = "";
					m_meshRenderer.material = ApplicationManager.assets.colorThemes[GameManager.Instance.colorThemeIndex].pieChartPartColorMaterials[0];
					break;
				case PieChartPartType.Multiplication:
					m_textMeshPro.text = "x" + m_counterUpgrade.ToString();
					m_meshRenderer.material = ApplicationManager.assets.colorThemes[GameManager.Instance.colorThemeIndex].pieChartPartColorMaterials[1];
					ChangeColorOpacity(0.65f);
					break;
				case PieChartPartType.Divison:
					m_textMeshPro.text = "/" + m_counterUpgrade.ToString();
					m_meshRenderer.material = ApplicationManager.assets.colorThemes[GameManager.Instance.colorThemeIndex].pieChartPartColorMaterials[2];
					break;
				case PieChartPartType.Addition:
					m_textMeshPro.text = "+" + m_counterUpgrade.ToString();
					m_meshRenderer.material = ApplicationManager.assets.colorThemes[GameManager.Instance.colorThemeIndex].pieChartPartColorMaterials[1];
					ChangeColorOpacity(0.65f);
					break;
				case PieChartPartType.Soustraction:
					m_textMeshPro.text = "-" + m_counterUpgrade.ToString()/*""*/;
					m_meshRenderer.material = ApplicationManager.assets.colorThemes[GameManager.Instance.colorThemeIndex].pieChartPartColorMaterials[2];
					ChangeColorOpacity(m_startValueColorOpacity);
					m_currentCounterUpgrade = m_counterUpgrade;
					break;
				case PieChartPartType.Empty:
					m_textMeshPro.text = "";
					break;
			}
		}

		private void ChangeColorOpacity(float opacityValue)
		{
			m_meshRenderer.material.SetFloat("_Opacity", opacityValue);
		}

	}

}