﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace Pinpin
{
	public class GeneratedLevel : Level
	{
		[Header("Generated Level")]
		[SerializeField] private int m_pieChartCount = 15;
		[SerializeField] private float m_spaceBetweenPieChart = 2f;
		[SerializeField] private bool m_isRandomPieChartRotation = false;
		[SerializeField] private LevelDificulty m_levelDifficulty = LevelDificulty.Easy;

		private enum LevelDificulty
		{
			Easy,
			Medium,
			Hard,
		}


		protected override void Awake ()
		{
			base.Awake();
			GenerateLevel();
		}

		protected override void Start ()
		{
			base.Start();
		}

		protected override void OnDestroy ()
		{
			base.OnDestroy();
		}

		protected override void FixedUpdate ()
		{
			base.FixedUpdate();
		}

		protected override void Update ()
		{
			base.Update();
		}

		protected override void StartGame ()
		{
			base.StartGame();
		}

		private void GenerateLevel ()
		{
			m_levelToRotate.transform.localScale = new Vector3(m_levelToRotate.transform.localScale.x, m_pieChartCount * 2f, m_levelToRotate.transform.localScale.z);
			Vector3 posToInstantiate = Vector3.zero;
			PieChartElement pieChartElement = null;
			int BadPieCharElementIndex = UnityEngine.Random.Range(5, m_pieChartCount);

			for (int i = 0; i < m_pieChartCount; i++)
			{
				if (i == 0)
					posToInstantiate = Vector3.zero;
				else
					posToInstantiate = m_pieChartElements[i - 1].transform.position + Vector3.down * m_spaceBetweenPieChart;

				if (i == 0)
					pieChartElement = ApplicationManager.assets.FirstPieChartElement[UnityEngine.Random.Range(0, ApplicationManager.assets.FirstPieChartElement.Length)];
				else if (m_levelDifficulty == LevelDificulty.Easy)
				{
					if (i == BadPieCharElementIndex)
						pieChartElement = ApplicationManager.assets.mediumPieChartElement[UnityEngine.Random.Range(0, ApplicationManager.assets.mediumPieChartElement.Length)];
					else
						pieChartElement = ApplicationManager.assets.easyPieChartElement[UnityEngine.Random.Range(0, ApplicationManager.assets.easyPieChartElement.Length)];
				}
				else if (m_levelDifficulty == LevelDificulty.Medium)
				{
					pieChartElement = ApplicationManager.assets.mediumPieChartElement[UnityEngine.Random.Range(0, ApplicationManager.assets.mediumPieChartElement.Length)];
				}
				else if (m_levelDifficulty == LevelDificulty.Hard)
					pieChartElement = ApplicationManager.assets.hardPieChartElement[UnityEngine.Random.Range(0, ApplicationManager.assets.hardPieChartElement.Length)];

				pieChartElement = Instantiate(pieChartElement, posToInstantiate, Quaternion.identity);

				pieChartElement.transform.SetParent(m_levelToRotate);
				if (m_isRandomPieChartRotation && i != 0)
					pieChartElement.transform.eulerAngles = new Vector3(0f, UnityEngine.Random.Range(0f, 360f), 0f);

				m_pieChartElements.Add(pieChartElement);
			}
			posToInstantiate = m_pieChartElements[m_pieChartElements.Count - 1].transform.position + Vector3.down * m_spaceBetweenPieChart;

			Finisher finisher = Instantiate(ApplicationManager.assets.finisher[UnityEngine.Random.Range(0, ApplicationManager.assets.finisher.Length)], posToInstantiate, Quaternion.identity);
			finisher.transform.SetParent(transform);
		}
	}
}
