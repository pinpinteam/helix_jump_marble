﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{
	public class FinisherEndPart : MonoBehaviour
	{
		private int m_ballCountEnter = 0;
		private bool m_isWin = false;

		private void OnTriggerEnter ( Collider other )
		{
			if (other.CompareTag("Ball"))
			{
				other.GetComponent<Ball>().SetGravityFinisher(false);
				m_ballCountEnter++;
				if(m_ballCountEnter == GameManager.Instance.currentLevel.totalBallsCount && !m_isWin)
				{
					m_isWin = true;
					StartCoroutine(WaitToWin());
				}
			}
		}

		private IEnumerator WaitToWin ()
		{
			yield return new WaitForSeconds(1f);
			GameManager.Instance.LevelWon();
		}

	}
}
