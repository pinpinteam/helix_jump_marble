﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using MoreMountains.NiceVibrations;
using System;

namespace Pinpin
{
	public class FinisherFxExplo : MonoBehaviour
	{
		public static Action<ulong, ulong> OnScoreChangeAction;
		[SerializeField] private Material fxMaterial;

		[SerializeField] private ParticleSystem[] m_fxRewardPachinko;
		[SerializeField] private TextMeshProUGUI m_textReward = null;
		[SerializeField] private int m_rewardValue = 1;

		private bool m_animBumpIsRunning = false;

		private void Awake ()
		{
			foreach(ParticleSystem fx in m_fxRewardPachinko)
			{
				ParticleSystem.MainModule main = fx.main;
				main.startColor = fxMaterial.color;
			}
			setReward();
		}

		private void OnTriggerEnter ( Collider other )
		{
			if (other.CompareTag("Ball"))
			{
				MMVibrationManager.Haptic(HapticTypes.MediumImpact);
				m_fxRewardPachinko[0].Play();
				startAnimBumpReward();
				ulong oldScore = GameManager.Instance.currentLevel.currentScoreFinisher;
				GameManager.Instance.currentLevel.currentScoreFinisher += (ulong)m_rewardValue;
				OnScoreChangeAction?.Invoke(oldScore, GameManager.Instance.currentLevel.currentScoreFinisher);
			}
		}

		private void setReward ()
		{
			m_textReward.text = "x" + m_rewardValue.ToString();
		}

		public void startAnimBumpReward ()
		{
			if (!m_animBumpIsRunning)
				StartCoroutine(AnimBump());
		}

		private IEnumerator AnimBump ()
		{
			m_animBumpIsRunning = true;
			float startTime = Time.time;
			while(Time.time < startTime + 0.5f)
			{
				m_textReward.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.one * 1.2f, ApplicationManager.config.game.animCurveBump.Evaluate((Time.time - startTime) / 0.5f));
				yield return null;
			}
			m_textReward.transform.localScale = Vector3.one;
			m_animBumpIsRunning = false;
		}
	}

}
