﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System;

namespace Pinpin
{
	public class Finisher : MonoBehaviour
	{
		public static Action OnEnterFinisherAction;

		[SerializeField] private CinemachineVirtualCamera m_finisherVirtualCam1 = null;
		[SerializeField] private CinemachineVirtualCamera m_finisherVirtualCam2 = null;

		[SerializeField] private Transform[] m_tubeStartPos;

		[SerializeField] private MeshRenderer[] m_pachinkoMeshToColor;
		[SerializeField] private MeshRenderer m_pachinkoBaseMeshToColor;
		[SerializeField] private MeshRenderer m_pachinoContainerMeshToColor;

		private void Awake ()
		{
			foreach(MeshRenderer meshRend in m_pachinkoMeshToColor)
				meshRend.material = ApplicationManager.assets.colorThemes[GameManager.Instance.colorThemeIndex].rotationCylinder;

			Material[] mats = m_pachinkoBaseMeshToColor.materials;
			mats[1] = ApplicationManager.assets.colorThemes[GameManager.Instance.colorThemeIndex].rotationCylinder;
			m_pachinkoBaseMeshToColor.materials = mats;

			mats = m_pachinoContainerMeshToColor.materials;
			mats[7] = ApplicationManager.assets.colorThemes[GameManager.Instance.colorThemeIndex].rotationCylinder;
			m_pachinoContainerMeshToColor.materials = mats;
		}

		private void OnTriggerEnter ( Collider other )
		{
			if (other.CompareTag("Ball"))
			{
				Ball ball = other.GetComponent<Ball>();

				ball.BallEnterFinisher();
				//ball.SetGravityFinisher();
				if(GameManager.Instance.gameState == GameManager.GameState.InGame)
				{
					GameManager.Instance.gameState = GameManager.GameState.Finisher;
					StartCoroutine(CamTransitionToPachinko());
					StartCoroutine(BallMovementOnFinisher());
					OnEnterFinisherAction?.Invoke();
				}
				//GameManager.Instance.LevelWon();
			}
		}

		private IEnumerator BallMovementOnFinisher ()
		{
			yield return new WaitForSeconds(2f);
			StartCoroutine(GameManager.Instance.currentLevel.MoveBallFinisher(m_tubeStartPos));
		}

		private IEnumerator CamTransitionToPachinko ()
		{
			m_finisherVirtualCam1.Priority = 50;
			yield return new WaitForSeconds(1.5f);
			m_finisherVirtualCam2.Priority = 100;
		}
	}

}
