﻿using DG.Tweening;
using UnityEngine;


namespace Pinpin
{
	public class TutoCanvas : MonoBehaviour
	{
		[SerializeField] private CanvasGroup m_canvasGroup;

		private void Start ()
		{
			gameObject.SetActive(true);
			//TODO change with your tuto deactivation
			//EnemyManager.onFirstInputAction += StartHide;

			m_canvasGroup.alpha = 0f;
			m_canvasGroup.DOFade(1f, 0.5f);
		}

		private void OnDestroy ()
		{
			//EnemyManager.onFirstInputAction -= StartHide;
		}


		void StartHide()
		{
			m_canvasGroup.DOFade(0f, 0.5f).onComplete += OnHideComplete;
		}

		void OnHideComplete()
		{
			gameObject.SetActive(false);
		}

	}
}
