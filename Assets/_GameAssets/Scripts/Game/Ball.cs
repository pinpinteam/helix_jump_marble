﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Pinpin
{
    public class Ball : MonoBehaviour
    {
        [HideInInspector] public float currentSpeed;
        [HideInInspector] public bool isMainBall = false;

        [SerializeField] private ParticleSystem[] m_ballSpawnParticle = null;
        [SerializeField] private ParticleSystem m_ballDispawnParticle = null;
        [SerializeField] private SphereCollider m_sphereCollider = null;
        [SerializeField] private Rigidbody m_rigidbody;
        [SerializeField] private TrailRenderer m_trailRend;

        public MeshRenderer meshRenderer;
        private float m_startScale;
        private bool m_canMove = true;
        private float m_powerBounceSpeed;

        private GameConfig.GameSettings gameConfig
        {
            get { return ApplicationManager.config.game; }
        }

		private void Awake ()
		{
            SetColor();
            m_ballSpawnParticle[0].Play();
            m_startScale = transform.localScale.x;
            transform.localScale = Vector3.zero;
            transform.DOScale(m_startScale, 0.5f);
            m_powerBounceSpeed = Random.Range(gameConfig.minBumpPower, gameConfig.maxBumpPower);
        }

        private void SetColor ()
		{
            ParticleSystem.MainModule main;
            foreach (ParticleSystem fx in m_ballSpawnParticle)
			{
                main = fx.main;
                main.startColor = ApplicationManager.assets.colorThemes[GameManager.Instance.colorThemeIndex].ballMaterial.color;
            }
            main = m_ballDispawnParticle.main;
            main.startColor = ApplicationManager.assets.colorThemes[GameManager.Instance.colorThemeIndex].ballMaterial.color;

            meshRenderer.material = ApplicationManager.assets.colorThemes[GameManager.Instance.colorThemeIndex].ballMaterial;
            m_trailRend.material.SetColor("_Color", ApplicationManager.assets.colorThemes[GameManager.Instance.colorThemeIndex].ballMaterial.color);
        }

		void Start ()
        {

        }

        void FixedUpdate ()
        {
            BallMovement();
        }

        private void BallMovement ()
		{
            if (!m_canMove) return;
            currentSpeed += -gameConfig.ballAccelerationSpeed * Time.fixedDeltaTime;
            if (currentSpeed <= -gameConfig.ballMaxSpeed)
                currentSpeed = -gameConfig.ballMaxSpeed;
            transform.position += Vector3.up * currentSpeed;
        }

		private void OnTriggerEnter ( Collider other )
		{
			if (other.CompareTag("PieChartPart"))
			{
                PieChartPart pieChartPart = other.attachedRigidbody.GetComponent<PieChartPart>();
                if (pieChartPart.pieChartPartType == PieChartPart.PieChartPartType.Basic)
                {
                    if (currentSpeed < 0)
                        currentSpeed = m_powerBounceSpeed;
                }
            }
		}

        public void BallEnterFinisher ()
		{
            m_canMove = false;
		}

        public void AnimationDispawnBall ()
		{
            m_canMove = false;
            m_sphereCollider.enabled = false;
            meshRenderer.enabled = false;
            m_ballDispawnParticle.Play();
            Destroy(gameObject, 1f);
        }

        public void SetGravityFinisher (bool isStartFinisher)
		{
            m_rigidbody.useGravity = isStartFinisher;
            m_sphereCollider.isTrigger = !isStartFinisher;
			m_trailRend.gameObject.SetActive(false);
		}
	}

}