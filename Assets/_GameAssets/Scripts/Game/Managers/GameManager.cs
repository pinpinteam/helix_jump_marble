﻿using PaperPlaneTools;
using Pinpin.Helpers;
using Pinpin.Scene.MainScene.UI;
using System;
using System.Collections;
using UnityEngine;
using MoreMountains.NiceVibrations;
#if TAPNATION
using FunGames.Sdk.Analytics;
#endif

namespace Pinpin
{

	public class GameManager : Singleton<GameManager>
	{

		#region members
		public static Action OnLevelLoaded;
		public static Action<bool> OnLevelEnd;
		public static Action OnLevelStart;
		public static Action<GameState> onStateChange;

		public Level currentLevel;
		private int m_currentLevelIndex;
		public bool canInput;
		[HideInInspector] public int colorThemeIndex = 0;

		public enum GameState
		{
			InMenu,
			InGame,
			Finisher,
			EndGame,
		}

		private GameState m_gameState;
		public GameState gameState
		{
			get
			{
				return m_gameState;
			}
			set
			{
				m_gameState = value;
				onStateChange?.Invoke(m_gameState);
			}
		}


		private bool m_loading = false;
		private bool m_startGameOnLoad = false;
		#endregion

		#region MonoBehavior
		private new void Awake ()
		{
			//ApplicationManager.onRewardedVideoAvailabilityChange += OnRewardedVideoAvailabilityChange;
			base.Awake();
		}

		// Use this for initialization
		void Start ()
		{
			LoadNextLevel();
		}

		private void OnDestroy ()
		{
			//ApplicationManager.onRewardedVideoAvailabilityChange -= OnRewardedVideoAvailabilityChange;
		}
		private void Update ()
		{
			if (Input.GetKeyDown(KeyCode.T))
				ApplicationManager.datas.haveCompleteTutorial = false;
		}

		#endregion

		private void OnRewardedVideoAvailabilityChange ( bool value )
		{

		}

		#region Level Callbacks
		public void LevelWon ()
		{
#if TAPNATION
			FunGamesAnalytics.NewProgressionEvent("Complete", ApplicationManager.datas.level.ToString());
#endif
			if (m_currentLevelIndex == ApplicationManager.datas.levelProgression)
			{
				ApplicationManager.datas.levelProgression++;
				ApplicationManager.datas.postGameLevelDoneCount = 0;
			}
			else
			{
				ApplicationManager.datas.postGameLevelDoneCount++;
			}
			ApplicationManager.datas.totalLevelDoneCount++;

			OnLevelEnd.Invoke(true);

			MMVibrationManager.Haptic(HapticTypes.Success); 
			gameState = GameState.EndGame;

			FXManager.Instance.PlayConffeti();
		}

		public void LevelFailed ()
		{
#if TAPNATION
			FunGamesAnalytics.NewProgressionEvent("Fail", ApplicationManager.datas.level.ToString());
#endif

			MMVibrationManager.Haptic(HapticTypes.Failure);
			gameState = GameState.EndGame;

			OnLevelEnd.Invoke(false);
		}
		#endregion

		#region GAME

		public void LoadNextLevel ()
		{
			int targetLevelIndex;
			if (ApplicationManager.datas.levelProgression < ApplicationManager.assets.levels.Length)
				targetLevelIndex = ApplicationManager.datas.levelProgression;
			else
			{
				//int levelOffset = ApplicationManager.config.game.onBoardingLevelToSkipCount < ApplicationManager.assets.levels.Length ? ApplicationManager.config.game.onBoardingLevelToSkipCount : 0;
				targetLevelIndex = -1; // levelOffset + (ApplicationManager.datas.postGameLevelDoneCount % (ApplicationManager.assets.levels.Length - levelOffset));
			}

			colorThemeIndex = Mathf.FloorToInt((ApplicationManager.datas.totalLevelDoneCount) / 4) % ApplicationManager.assets.colorThemes.Length;
#if CAPTURE
			if (CheatMenu.Instance.isColorThemeChange)
				colorThemeIndex = CheatMenu.Instance.newcolorThemeIndex;
#endif

			LoadLevel(targetLevelIndex);
			gameState = GameState.InMenu;
		}
		public void RemoveCurrentLevel ()
		{
			if (currentLevel != null)
				Destroy(currentLevel.gameObject);
		}

		public void LoadLevel ( int levelIndex )
		{
			if (m_loading)
				return;
			m_loading = true;

			StartCoroutine(LoadLevelCoroutine(levelIndex));
		}
		public void StartGame ()
		{
			gameState = GameState.InGame;
			OnLevelStart?.Invoke();
		}

		private IEnumerator LoadLevelCoroutine ( int levelIndex )
		{
			RemoveCurrentLevel();

			yield return null;

			m_loading = false;
			Level levelToInstantiate = null;

			m_loading = false;
			if (levelIndex >= 0 && levelIndex < ApplicationManager.assets.levels.Length)
				levelToInstantiate = ApplicationManager.assets.levels[levelIndex];
			else
				levelToInstantiate = ApplicationManager.assets.generatedLevel;

			m_currentLevelIndex = levelIndex;

			currentLevel = Instantiate(levelToInstantiate);


			yield return null;
			if (m_startGameOnLoad)
			{
				m_startGameOnLoad = false;
				StartGame();
			}
#if TAPNATION
			FunGamesAnalytics.NewProgressionEvent("Start", ApplicationManager.datas.level.ToString());
#endif
		}
		#endregion

		public void ChooseColorTheme ()
		{

		}

	}

}