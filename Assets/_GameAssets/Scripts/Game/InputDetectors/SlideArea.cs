using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Pinpin.InputControllers
{
	[DisallowMultipleComponent,
	RequireComponent(typeof(CanvasRenderer))]
	public class SlideArea : Graphic, IPointerDownHandler, IDragHandler, IPointerUpHandler
	{

		[SerializeField] private bool horizontalOnly = true;
		public Vector2 pointerDelta { get; private set; }
		public Vector2 delta { get; private set; }
		public int touchCount = 0;

		protected override void OnEnable()
		{
			base.OnEnable();
			touchCount = 0;
		}

		public bool moved
		{
			get { return (delta != Vector2.zero); }
		}

		public bool isMoving
		{
			get { return (touchCount > 0); }
		}


		protected override void Start()
		{
			pointerDelta = Vector2.zero;
			delta = Vector2.zero;
		}

		void Update()
		{
			if (Input.GetAxisRaw("Vertical") != 0f || Input.GetAxisRaw("Horizontal") != 0f)
				delta = new Vector2(Input.GetAxisRaw("Horizontal"), horizontalOnly ? 0f : Input.GetAxisRaw("Vertical")) * Time.deltaTime * 2f;
			else if (pointerDelta != Vector2.zero)
			{
				delta = pointerDelta;
				if (horizontalOnly)
					delta = new Vector2(delta.x, 0f);
				pointerDelta = Vector2.zero;
			}
			else
				delta = Vector2.zero;
		}

		public void OnPointerDown(PointerEventData ped)
		{
			touchCount++;
			pointerDelta = Vector2.zero;
		}

		public void OnDrag(PointerEventData ped)
		{
			pointerDelta = ped.delta / Screen.height * 2f * Camera.main.orthographicSize;
		}

		public void OnPointerUp(PointerEventData ped)
		{
			if (touchCount > 0)
				touchCount--;
			pointerDelta = Vector2.zero;
		}
	}
}