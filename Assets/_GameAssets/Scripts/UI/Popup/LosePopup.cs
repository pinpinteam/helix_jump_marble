﻿using UnityEngine;
using Pinpin.UI;

namespace Pinpin.Scene.MainScene.UI
{

	public sealed class LosePopup : AClosablePopup
	{
		[SerializeField] private GameObject m_blockClickGO;

		private bool CanClick
		{
			get
			{
				return !m_blockClickGO.activeSelf;
			}
			set
			{
				m_blockClickGO.SetActive(!value);
			}
		}

		protected override void OnEnable ()
		{
			base.OnEnable();
			CanClick = true;
		}

		private new MainSceneUIManager UIManager
		{
			get { return (base.UIManager as MainSceneUIManager); }
		}

		protected override void OnClose ()
		{
			base.OnClose();

			FXManager.onFadeInFinishedAction += OnFadeInRetry;
			FXManager.Instance.FadeIn();
		}

		void OnFadeInRetry ()
		{
			//GamePanel gp = UIManager.GetPanel<GamePanel>();
			//int levelIndex = ApplicationManager.datas.level % ApplicationManager.assets.levels.Length;
			//gp.Init(levelIndex);

			gameObject.SetActive(false);

			UIManager.OpenPanel<MainPanel>();
			UIManager.SetCurrentPanelAsRoot();

			GameManager.Instance.LoadNextLevel();

			FXManager.Instance.FadeOut();
			// UIManager.OpenPanel<MainPanel>();
			// UIManager.sceneManager.gameManager.RemoveCurrentLevel();
		}
	}
}