﻿using UnityEngine;
using Pinpin.UI;
using System.Collections.Generic;
using System.Collections;
using DG.Tweening;
using PaperPlaneTools;

namespace Pinpin.Scene.MainScene.UI
{
	public sealed class WinPopup : AClosablePopup
	{
		[Header("BTNs")]
		[SerializeField] private PushButton m_normalCollectBtn;
		[SerializeField] private PushButton m_RVCollectBtn;
		//[SerializeField] private PushButton m_VIPCollectBtn;
		//[SerializeField] private PushButton m_noThanksBtn;
		[Space()]
		[SerializeField] private RectTransform m_btnContainer;
		[SerializeField] private CanvasGroup m_btnContainerCanvasGr;


		[Header("CanClick")]
		[SerializeField] private GameObject m_blockClickGO;

		private Tweener m_RVscaleTween;

		private bool CanClick
		{
			get
			{
				return !m_blockClickGO.activeSelf;
			}
			set
			{
				m_blockClickGO.SetActive(!value);
			}
		}

		public void Init ()
		{
			CanClick = true;

			UIManager.ShowTopCanvas<MoneyTopCanvas>();
			int score = (int)GameManager.Instance.currentLevel.currentScoreFinisher;
			m_normalCollectBtn.text = score.ToString();
			m_normalCollectBtn.gameObject.SetActive(false);
			m_RVscaleTween.Pause();
			m_RVscaleTween.Kill();
			m_RVCollectBtn.transform.localScale = Vector3.one * 0.75f;
			m_RVCollectBtn.text = (score * 3).ToString();
			m_RVCollectBtn.gameObject.SetActive(false);

			m_btnContainer.anchoredPosition = Vector3.up * -150f;
			m_btnContainerCanvasGr.alpha = 0f;
			m_btnContainer.gameObject.SetActive(false);

			StartCoroutine(DisplayCounterCR());
		}

		protected override void Awake ()
		{
			base.Awake();

			m_normalCollectBtn.onClick += OnClickCollect;
			m_RVCollectBtn.onClick += OnClickRVCollect;
		}

		protected override void OnDestroy ()
		{
			base.OnDestroy();
			m_normalCollectBtn.onClick -= OnClickCollect;
			m_RVCollectBtn.onClick -= OnClickRVCollect;
		}

		private new MainSceneUIManager UIManager
		{
			get { return (base.UIManager as MainSceneUIManager); }
		}

		protected override void OnClose ()
		{
			base.OnClose();

			FXManager.onFadeInFinishedAction += OnFadeInLaunchNextLevel;
			FXManager.Instance.FadeIn();
		}

		void OnFadeInLaunchNextLevel ()
		{
#if TAPNATION
			if (!UIManager.sceneManager.ShowInterstitial() && ApplicationManager.datas.levelProgression > 4)
			{
#if UNITY_IOS
				RateBox.Instance.ShowIOSNativeRewiew();
#elif UNITY_ANDROID
				RateBox.Instance.Show();
#endif
			}
#endif
			FXManager.Instance.FadeOut();
			UIManager.OpenPanel<MainPanel>();
			UIManager.SetCurrentPanelAsRoot();

			GameManager.Instance.LoadNextLevel();
		}

		void OnClickCollect ()
		{
			if (!CanClick) return;
			StartCoroutine(CollectCoroutine( m_normalCollectBtn));
		}

		void OnClickRVCollect ()
		{
			if (!CanClick) return;
#if !TAPNATION
			StartCoroutine(CollectCoroutine(m_gain * 3f, m_RVCollectBtn));
#else
			UIManager.sceneManager.ShowRewardedVideo(OnCollectGoldRV, "RV_WinPopUp_*3");
#endif
		}

		IEnumerator CollectCoroutine ( PushButton btn )
		{
			CanClick = false;

			m_RVscaleTween.Pause();
			m_RVscaleTween.Kill();

			float t = 1f;
			float m_currentCoinDisplayed = GameManager.Instance.currentLevel.currentScoreFinisher;

			float startCoin = m_currentCoinDisplayed;

			ApplicationManager.AddSoftCurrency((ulong)m_currentCoinDisplayed);

			btn.linkedParticleSystem.Play();

			while (t > 0f)
			{
				t -= Time.deltaTime;
				m_currentCoinDisplayed = Mathf.Round(startCoin * t);
				if (m_currentCoinDisplayed >= 0f)
					btn.text = m_currentCoinDisplayed.ToString();
				else
					t = 0f;
				yield return null;
			}

			btn.text = "0";

			btn.linkedParticleSystem.Stop();

			yield return new WaitForSeconds(0.4f);
			Close();
		}

		private void OnCollectGoldRV ( AdsManagement.RewardedVideoEvent adEvent )
		{
			switch (adEvent)
			{
				case AdsManagement.RewardedVideoEvent.Rewarded:
					StartCoroutine(CollectCoroutine(m_RVCollectBtn));
					break;

				case AdsManagement.RewardedVideoEvent.Failed:
					CanClick = true;
					break;
			}
		}

		IEnumerator DisplayCounterCR ()
		{
			yield return new WaitForSeconds(1.5f);

			OnInfoDisplayFinished();
		}

		private void OnInfoDisplayFinished ()
		{

			m_RVCollectBtn.gameObject.SetActive(true);

			if (m_RVCollectBtn.isInteractable)
				m_RVscaleTween = m_RVCollectBtn.transform.DOScale(1f, 1f).SetLoops(-1, LoopType.Yoyo);

			StartCoroutine(DelayNormalCollectAppearanceCR());
			ShowCollectBtns(0.5f);
		}

		IEnumerator DelayNormalCollectAppearanceCR ()
		{
			float t = 3.5f; //normalCollectBtnDelay

			while (t > 0f && gameObject.activeSelf)
			{
				if (CanClick)
					t -= Time.deltaTime;
				yield return null;
			}

			m_normalCollectBtn.gameObject.SetActive(true);
		}

		void ShowCollectBtns ( float delay )
		{
			m_btnContainer.gameObject.SetActive(true);
			m_btnContainer.DOAnchorPosY(0f, 0.5f).SetDelay(delay);
			m_btnContainerCanvasGr.DOFade(1f, 0.5f).SetDelay(delay);
		}
	}

}