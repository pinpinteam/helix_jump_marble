﻿using Pinpin.UI;
using UnityEngine;
using UnityEngine.UI;
using Pinpin.Helpers;
using DG.Tweening;

namespace Pinpin.Scene.MainScene.UI
{
	public sealed class GamePanel : AUIPanel
	{
		[SerializeField] private RectTransform m_topContainer;
		[SerializeField] private CurrencyCounter m_currentGainCounter;
		[SerializeField] private CurrencyCounter m_ScoreGainCounter;
		[SerializeField] private RectTransform m_bottomContainer;
		[SerializeField] private Text m_LevelCounterTxt;
		//[SerializeField] private 

		private float m_topContainerYPos;
		private float m_bottomContainerYPos;

		private new MainSceneUIManager UIManager
		{
			get { return (base.UIManager as MainSceneUIManager); }
		}

		public override bool OnBackAction()
		{
			return false;
		}

		protected override void Awake ()
		{
			base.Awake();
			GameManager.OnLevelEnd += OnLevelEnd;
			Level.onBallCountChangedAction += OnBallCountChange;
			Finisher.OnEnterFinisherAction += OnFinisherEnter;
			FinisherFxExplo.OnScoreChangeAction += OnScoreCountChange;

			m_topContainerYPos = m_topContainer.anchoredPosition.y;
			m_bottomContainerYPos = m_bottomContainer.anchoredPosition.y;
			m_currentGainCounter.Init();
		}

		protected override void OnDestroy ()
		{
			base.OnDestroy(); 
			GameManager.OnLevelEnd -= OnLevelEnd; 
			Level.onBallCountChangedAction -= OnBallCountChange;
			Finisher.OnEnterFinisherAction -= OnFinisherEnter;
			FinisherFxExplo.OnScoreChangeAction -= OnScoreCountChange;
		}

		void OnEnable ()
		{
			m_topContainer.anchoredPosition = new Vector2(0, m_topContainerYPos + 200f);
			m_bottomContainer.anchoredPosition = new Vector2(0, m_bottomContainerYPos - 200f);

			m_topContainer.DOAnchorPosY(m_topContainerYPos, 0.5f).SetEase(Ease.OutQuad);
			m_bottomContainer.DOAnchorPosY(m_bottomContainerYPos, 0.5f).SetEase(Ease.OutQuad);

			m_currentGainCounter.ResetToHiddenPos();
			m_currentGainCounter.SetCurrencyInstant(0);
			m_currentGainCounter.SetVisible(true);
			OnBallCountChange(0, (ulong)GameManager.Instance.currentLevel.totalBallsCount);
			m_currentGainCounter.SetColorIcon(ApplicationManager.assets.colorThemes[GameManager.Instance.colorThemeIndex].ballMaterial.color);

			m_ScoreGainCounter.ResetToHiddenPos();
			m_ScoreGainCounter.SetCurrencyInstant(0);

			m_LevelCounterTxt.text = "LEVEL " + (ApplicationManager.datas.totalLevelDoneCount + 1);
		}

		public void Init ( int levelIndex )
		{
		}

		void OnLevelEnd ( bool win )
		{
			m_currentGainCounter.SetVisible(false);
			if (win)
			{
				UIManager.OpenPopup<WinPopup>().Init();
			}
			else
			{
				UIManager.OpenPopup<LosePopup>();
			}
		}

		#region Game
		private void OnBallCountChange ( ulong oldValue, ulong newValue )
		{
			m_currentGainCounter.UpdateDisplay(oldValue, newValue);
		}

		public void OnFinisherEnter ()
		{
			m_currentGainCounter.SetVisible(false);
			m_ScoreGainCounter.SetVisible(true);
		}

		private void OnScoreCountChange ( ulong oldValue, ulong newValue )
		{
			m_ScoreGainCounter.UpdateDisplay(oldValue, newValue);
		}
		#endregion
	}
}