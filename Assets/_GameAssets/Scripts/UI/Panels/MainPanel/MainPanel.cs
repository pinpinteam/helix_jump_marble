﻿using AdsManagement;
using Pinpin.UI;
using UnityEngine;
using UnityEngine.UI;
using Pinpin.Helpers;
using DG.Tweening;

using UVector3 = UnityEngine.Vector3;
using System;
using System.Collections;
using URandom = UnityEngine.Random;
using System.Collections.Generic;
using Pinpin.InputControllers;

namespace Pinpin.Scene.MainScene.UI
{
	public sealed class MainPanel : AUIPanel
	{
		#region members
		[SerializeField] private Canvas m_cheatCanvas;

		[Header("Animation")]
		[SerializeField] Text m_startText;
		[SerializeField] private Image m_modalImg;
		[SerializeField] private RectTransform m_bottomContainerRectTfm;

		[SerializeField] private GameObject m_blockClickGO;


		private Tweener m_bottomContainerTween;
		private Vector2 m_startBtnPos;
		private Color m_modalColor;
		private Tweener m_textAlphaTween;

		private Vector2 m_slideCount = Vector2.zero;
		private float m_distanceSlide;

		private bool CanClick
		{
			get
			{
				return !m_blockClickGO.activeInHierarchy;
			}
			set
			{
				m_blockClickGO.SetActive(!value);
			}
		}

		private new MainSceneUIManager UIManager
		{
			get { return (base.UIManager as MainSceneUIManager); }
		}
		#endregion

		#region MonoBehavior
		protected override void Awake ()
		{
			base.Awake();


			m_startBtnPos = m_bottomContainerRectTfm.anchoredPosition;
			m_modalColor = m_modalImg.color;
		}

		void Start ()
		{
#if CAPTURE || UNITY_EDITOR
			m_cheatCanvas.gameObject.SetActive(true);
#else
			Destroy(m_cheatCanvas.gameObject);
#endif
		}

		private void OnEnable ()
		{
			GameManager.Instance.gameState = GameManager.GameState.InMenu;

			UIManager.ShowTopCanvas<MoneyTopCanvas>();

			m_textAlphaTween.Pause();
			m_textAlphaTween.Kill();
			m_startText.color = Color.white;
			m_textAlphaTween = m_startText.DOFade(0.5f, 1f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
			m_startText.rectTransform.localScale = Vector3.one;
			m_modalImg.color = m_modalColor;

			m_bottomContainerTween.Pause();
			m_bottomContainerTween.Kill();
			m_bottomContainerRectTfm.anchoredPosition = m_startBtnPos - Vector2.up * 500f;
			m_bottomContainerTween = m_bottomContainerRectTfm.DOAnchorPos(m_startBtnPos, 0.5f).SetEase(Ease.OutQuad);


			CanClick = true;
		}

		protected override void OnDestroy ()
		{
			base.OnDestroy();
			//ApplicationManager.onRewardedVideoAvailabilityChange -= OnRewardedVideoAvailabilityChange;
		}

		private void Update ()
		{
			sliderStartButton();
		}
		#endregion

		#region Buttons CallBacks
		private void sliderStartButton ()
		{
			if (GameManager.Instance.gameState == GameManager.GameState.InMenu)
			{
				if (InputManager.isTouching)
				{
					m_slideCount += InputManager.moveDelta;
					m_distanceSlide = Vector2.Distance(m_slideCount, Vector2.zero);
					if (m_distanceSlide > ApplicationManager.config.game.distanceSlideToStart)
					{
						OnStartButtonClick();
						m_slideCount = Vector2.zero;
					}
				}
				else
				{
					m_slideCount = Vector2.zero;
				}
			}
		}

		private void OnStartButtonClick ()
		{
			if (!CanClick) return;
			CanClick = false;

			UIManager.HideTopCanvas<MoneyTopCanvas>();

			m_modalImg.DOFade(0f, 0.3f);
			m_textAlphaTween.Pause();
			m_textAlphaTween.Kill();
			m_startText.color = Color.white;
			m_startText.rectTransform.DOPunchScale(Vector3.one * 0.2f, 0.35f);
			m_textAlphaTween = m_startText.DOFade(0f, 0.4f).SetEase(Ease.InQuad);

			m_bottomContainerTween.Pause();
			m_bottomContainerTween.Kill();
			m_bottomContainerTween = m_bottomContainerRectTfm.DOAnchorPos(m_startBtnPos - Vector2.up * 500f, 0.45f);

			m_bottomContainerTween.onComplete += OnMainMenuHidden;

		}

		void OnMainMenuHidden ()
		{
			UIManager.OpenPanel<GamePanel>();//.Init();
			GameManager.Instance.StartGame();
		}
		#endregion
	}
}