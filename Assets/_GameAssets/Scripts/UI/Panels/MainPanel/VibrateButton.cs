﻿using MoreMountains.NiceVibrations;
using Pinpin.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Pinpin.Scene.MainScene.UI {
	public class VibrateButton : MonoBehaviour {
		[SerializeField] Sprite m_vibration;
		[SerializeField] Sprite m_noVibration;
		[SerializeField] Image m_image;
		[SerializeField] PushButton m_pushButton; 

		private void Awake()
		{
			m_pushButton.onClick += OnVibrateButtonClick;
		}

		private void OnDestroy()
		{
			m_pushButton.onClick -= OnVibrateButtonClick;
		}

		private void Start()
		{
			ShowState();
		}

		private void OnVibrateButtonClick()
		{
			ApplicationManager.datas.isVibrationActive = !ApplicationManager.datas.isVibrationActive;
			MMVibrationManager.Haptic(HapticTypes.MediumImpact);
			ShowState();
		}

		private void ShowState()
		{
			if (ApplicationManager.datas.isVibrationActive)
			{
				m_image.sprite = m_vibration;
			}
			else
			{
				m_image.sprite = m_noVibration;
			}
		}
	}
}
