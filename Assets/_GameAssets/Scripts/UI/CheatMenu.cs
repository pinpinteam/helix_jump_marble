﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pinpin.Scene.MainScene.UI;
using Pinpin.UI;

namespace Pinpin
{
    public class CheatMenu : Singleton<CheatMenu>
    {
        [Space()]
        [SerializeField] private GameObject m_menuGO;
        public bool IsCheatMenuVisible
        {
            get
            {
                return m_menuGO.activeSelf;
            }
        }
        [Space()]
        [SerializeField] private PushButton m_hudBtn;
        [SerializeField] private Text m_currentLoadedLevelTxt;

        [Header("CHANGE COLOR THEME")]
        [SerializeField] private PushButton m_colorThemeLoadBtn;
        [SerializeField] private PushButton[] m_colorThemeLRBtns;
        [SerializeField] private Text m_currentcolorThemeText;
        [HideInInspector] public int newcolorThemeIndex = 0;
        [HideInInspector] public bool isColorThemeChange = false;

        [Space()]
        [SerializeField] private PushButton m_showBtn;
        [SerializeField] private PushButton m_quitBtn;

        public bool isHUDVisible = true;
        public GameObject[] hudGO;

        public bool isOpen = false;
#if CAPTURE
        public override void Awake ()
		{
			base.Awake();

            m_hudBtn.onClick += OnClickHudBtn;

            m_showBtn.onClick += OnClickShowBtn;
            m_quitBtn.onClick += OnClickQuit;

            m_colorThemeLoadBtn.onClick += OnClickColorThemeLoadBtn;
            m_colorThemeLRBtns[0].onClick += OnClickColorThemeLeftBtn;
            m_colorThemeLRBtns[1].onClick += OnClickColorThemeRightBtn;
        }

        private void OnEnable ()
        {
            UpdateBtnStatus();
        }

        private void Start ()
        {
            UpdateBtnStatus();
        }

        private void UpdateBtnStatus ()
        {
            m_hudBtn.text = isHUDVisible ? "HUD : Visible" : "HUD : Hidden";
            m_currentcolorThemeText.text = ApplicationManager.assets.colorThemes[newcolorThemeIndex].name;

            if (gameObject.activeSelf)
                StartCoroutine(UpdateLevelNameCoroutine());
        }

        IEnumerator UpdateLevelNameCoroutine ()
        {
            //wait 2 frames cause the Load Level is a coroutine that wait 1 frame after removing the currentLevel
            yield return null;
            yield return null;
            m_currentLoadedLevelTxt.text = "CURRENT LEVEL : " + ApplicationManager.datas.totalLevelDoneCount.ToString();
        }

        void OnClickHudBtn ()
        {
            isHUDVisible = !isHUDVisible;
            UpdateBtnStatus();

            foreach (GameObject GO in hudGO)
                GO.SetActive(isHUDVisible);
        }

        void OnClickColorThemeLoadBtn ()
        {
            isColorThemeChange = true;
            GameManager.Instance.LoadNextLevel();
            //Load
            UpdateBtnStatus();
        }

        void OnClickColorThemeLeftBtn ()
        {
            newcolorThemeIndex = newcolorThemeIndex == 0 ? ApplicationManager.assets.colorThemes.Length - 1 : newcolorThemeIndex - 1;
            UpdateBtnStatus();
        }

        void OnClickColorThemeRightBtn ()
        {
            newcolorThemeIndex = (newcolorThemeIndex + 1) % ApplicationManager.assets.colorThemes.Length;
            UpdateBtnStatus();
        }


        void OnClickShowBtn ()
        {
            isOpen = true;
            UpdateBtnStatus();
            m_menuGO.SetActive(true);
        }

        void OnClickQuit ()
        {
            m_menuGO.SetActive(false);
        }
#endif
    }

}
