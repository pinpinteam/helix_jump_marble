using UnityEngine;
using Pinpin.Types;
using System;
using System.Numerics;
using System.Collections.Generic;

namespace Pinpin
{

	[CreateAssetMenu(fileName = "GameDatas", menuName = "Game/GameDatas", order = 1)]
	public partial class GameDatas: ScriptableObject
	{

		public static event Action onLoadComplete;
		[SerializeField, ShowOnly] private SavedDatas		m_savedDatas = new SavedDatas();
		[SerializeField, ShowOnly] private PlayerPrefDatas	m_playerPrefDatas = new PlayerPrefDatas();
		public int currentStage = 0;

		private bool dirty { get; set; }

		public uint firstApplicationLaunchTime
		{
			get { return (m_savedDatas.firstApplicationLaunchTime); }
			set
			{
				this.dirty = true;
				m_savedDatas.firstApplicationLaunchTime = value;
			}
		}

		public uint lastApplicationQuitTime
		{
			get { return (m_savedDatas.lastApplicationQuitTime); }
			set
			{
				this.dirty = true;
				m_savedDatas.lastApplicationQuitTime = value;
			}
		}

		public uint lastApplicationLaunchTime
		{
			get { return (m_savedDatas.lastApplicationLaunchTime); }
			set
			{
				this.dirty = true;
				m_savedDatas.lastApplicationLaunchTime = value;
			}
		}

		public uint consecutiveDaysLaunches
		{
			get { return (m_savedDatas.consecutiveDaysLaunches); }
			set
			{
				this.dirty = true;
				m_savedDatas.consecutiveDaysLaunches = value;
			}
		}
		public uint sessionCount
		{
			get { return (m_savedDatas.sessionCount); }
			set
			{
				this.dirty = true;
				m_savedDatas.sessionCount = value;
			}
		}

		public bool enableOENotifications
		{
			get { return (m_playerPrefDatas.enableOENotifications); }
			set
			{
				this.dirty = true;
				m_playerPrefDatas.enableOENotifications = value;
			}
		}

		public bool isSoundActive
		{
			get { return (m_playerPrefDatas.soundActive); }
			set
			{
				this.dirty = true;
				m_playerPrefDatas.soundActive = value;
				ApplicationManager.assets.audioMixer.SetFloat("Master", value ? 0f : -80f);
			}
		}

		public bool isVibrationActive
		{
			get { return (m_playerPrefDatas.vibrationActive); }
			set
			{
				this.dirty = true;
				m_playerPrefDatas.vibrationActive = value;
			}
		}

		public float musicVolume
		{
			get { return (m_playerPrefDatas.musicVolume); }
			set
			{
				this.dirty = true;
				m_playerPrefDatas.musicVolume = value;
				ApplicationManager.assets.audioMixer.SetFloat("Music", -80f + value * 80f);
			}
		}
		
		public float sfxVolume
		{
			get { return (m_playerPrefDatas.sfxVolume); }
			set
			{
				this.dirty = true;
				m_playerPrefDatas.sfxVolume = value;
				ApplicationManager.assets.audioMixer.SetFloat("Sfx", -80f + value * 80f);
			}
		}

		public bool subscribed
		{
			get { return (m_savedDatas.subscribed); }
			set
			{
				this.dirty = true;
				m_savedDatas.subscribed = value;
			}
		}

		public bool vip
		{
			get { return (m_savedDatas.vip); }
			set
			{
				this.dirty = true;
				m_savedDatas.vip = value;
			}
		}



		public ulong softCurrency
		{
			get { return (m_savedDatas.softCurrency); }
			set
			{
				this.dirty = true;
				m_savedDatas.softCurrency = value;
			}
		}
		
		public ulong hardCurrency
		{
			get { return (m_savedDatas.hardCurrency); }
			set
			{
				this.dirty = true;
				m_savedDatas.hardCurrency = value;
			}
		}
		

		public ulong xp
		{
			get { return (m_savedDatas.xp); }
			set
			{
				this.dirty = true;
				m_savedDatas.xp = value;
			}
		}
		
		public int level
		{
			get { return (m_savedDatas.level); }
			set
			{
				this.dirty = true;
				m_savedDatas.level = value;
			}
		}


		//Game Progression
		public int levelProgression
		{
			get { return (m_savedDatas.levelProgression); }
			set
			{
				this.dirty = true;
				m_savedDatas.levelProgression = value;
			}
		}
		public int postGameLevelDoneCount
		{
			get { return (m_savedDatas.postGameLevelDoneCount); }
			set
			{
				this.dirty = true;
				m_savedDatas.postGameLevelDoneCount = value;
			}
		}
		public int totalLevelDoneCount
		{
			get { return (m_savedDatas.totalLevelDoneCount); }
			set
			{
				this.dirty = true;
				m_savedDatas.totalLevelDoneCount = value;
			}
		}

		public bool haveCompleteTutorial
		{
			get { return (m_savedDatas.haveCompleteTutorial); }
			set
			{
				this.dirty = true;
				m_savedDatas.haveCompleteTutorial = value;
			}
		}

		public uint lifetimeCollect
		{
			get { return (m_savedDatas.lifetimeCollect); }
			set
			{
				this.dirty = true;
				m_savedDatas.lifetimeCollect = value;
			}
		}

		public int rateboxShown
		{
			get { return (m_savedDatas.rateboxShown); }
			set
			{
				this.dirty = true;
				m_savedDatas.rateboxShown = value;
			}
		}

		#region Loading

		public void LoadSettings ()
		{
			#if DEBUG
				Debug.Log("GameDatas - Loading settings");
			#endif

			// Load Music settings, Graphics, Language, etc
			
			// Load Sounds Settins
			this.isSoundActive = this.isSoundActive;
			this.musicVolume = this.musicVolume;
			this.sfxVolume = this.sfxVolume;
			
			this.dirty = false; // DO NOT After that

			#if DEBUG
				Debug.Log("GameDatas - Settings Loaded");
			#endif
		}
		
		public void LoadDatas ()
		{
			m_playerPrefDatas = PlayerPrefDatas.LoadDatas();
			SavedDatas.LoadCloudDatas(OnCloudDatasLoaded);
		}

		private void OnCloudDatasLoaded ( SavedDatas savedDatas )
		{
			m_savedDatas = savedDatas;
			if (m_savedDatas == null)
				m_savedDatas = SavedDatas.LoadDatas();
			else
				SavedDatas.SaveDatas(m_savedDatas);
			

			#if DEBUG
				Debug.Log("GameDatas - Datas Loaded");
			#endif

			if (GameDatas.onLoadComplete != null)
				GameDatas.onLoadComplete.Invoke();
		}
		
		public void SaveDatas ()
		{
			#if DEBUG
				Debug.Log("GameDatas - Saving");
			#endif

			if (dirty)
			{
				SavedDatas.SaveDatas(m_savedDatas);
				SavedDatas.SaveCloudDatas(m_savedDatas);
				m_playerPrefDatas.SaveDatas();
				dirty = false;

				#if DEBUG
					Debug.Log("GameDatas - Saved");
				#endif
			}
			else
			{
				#if DEBUG
					Debug.Log("GameDatas - Already Up to date");
				#endif
			}
		}

		#endregion


	}

}