﻿using Pinpin.Helpers;
using System;
using UnityEngine;

namespace Pinpin
{

	[CreateAssetMenu(fileName = "GameConfig", menuName = "Game/GameConfig", order = 1)]
	public class GameConfig: ScriptableObject
	{

		[Serializable]
		public class ApplicationConfig
		{
			public string version = "1.0";
			public int targetFrameRate = 60;
			public int splashScreenDuration;
			public bool enableRemoteSettings = false;
			public bool enablePurchasing = false;
			public bool enableAds = false;
			public bool enableOfflineEarning;
			public string amplitudeAPIKey;
		}

		[Serializable]
		public sealed class AdsSettings
		{
			public enum Banner
			{
				None,
				Bottom,
				Top
			}

			public int				lifetimeCollectBeforeInterstitial = 1;
			[Min(0f)] public float	delayBetweenInterstitials = 0f;
			[Min(0f)] public float	delayFirstInterstitial = 0f;
			public Banner			banner;
			public bool				useSmartBanner = false;

			public string interstitialTestAdUnit;
			public string rewardedVideoTestAdUnit;

			public bool isFirstInterstitial { get; internal set; }
		}

		[Serializable]
		public sealed class GameSettings
		{
			public float fadeTransitionDuration = 0.5f;

			[Header("Game")]
			public float speedRotationLevel = 10f;
			public float ballAccelerationSpeed = 10f;
			public float ballMaxSpeed = 20f;
			public float minBumpPower = 5f;
			public float maxBumpPower = 5f;
			public float ballOffset = 0.1f;
			public float ballRandomOffSet = 0.02f;

			[Header("MainPanel")]
			public float distanceSlideToStart = 0.2f;

			[Header("Finisher")]
			public float durationBetweenBallMovePachinko = 0.2f;
			public AnimationCurve animCurveBump = null;

			[Header("Haptic")]
			public float lightHapticCooldown = 0.1f;

		}

		public ApplicationConfig	application;
		public AdsSettings			ads;
		public GameSettings			game;

		public void Inititialize ()
		{
			if (this.application.enableRemoteSettings)
				RemoteSettings.Updated += new RemoteSettings.UpdatedEventHandler(this.OnRemoteSettingsUpdate);
		}

		private void OnDisable ()
		{
			RemoteSettings.Updated -= new RemoteSettings.UpdatedEventHandler(this.OnRemoteSettingsUpdate);
		}

		private void OnRemoteSettingsUpdate ()
		{
			Debug.Log("GameConfig - OnRemoteSettingsUpdate()");
		}
	}
}