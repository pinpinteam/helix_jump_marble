﻿Shader "Hidden/ContrastDebugger"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Pass
        {
            CGPROGRAM
            #pragma vertex vert_img
            #pragma fragment frag

            #include "UnityCG.cginc"

            sampler2D _MainTex;
			float2 _ScreenRes;
			int _Width;
			float _Threshold;
			float4 _Color;

			// All components are in the range [0…1], including hue.
			float3 rgb2hsv(float3 c)
			{
				float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
				float4 p = lerp(float4(c.bg, K.wz), float4(c.gb, K.xy), step(c.b, c.g));
				float4 q = lerp(float4(p.xyw, c.r), float4(c.r, p.yzx), step(p.x, c.r));

				float d = q.x - min(q.w, q.y);
				float e = 1.0e-10;
				return float3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
			}

			float3 hsv2rgb(float3 c)
			{
				float4 K = float4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
				float3 p = abs(frac(c.xxx + K.xyz) * 6.0 - K.www);
				return c.z * lerp(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
			}

            fixed4 frag (v2f_img input) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, input.uv);
				float s = rgb2hsv(col).y;

				float2 uv = 0;
				float s2 = 0;
				float3 error = 0;

				for (int i = 1; i < _Width + 1; i++) 
				{
					uv = float2(input.uv.x + i / _ScreenRes.x, input.uv.y);
					col = tex2D(_MainTex, uv);
					s2 = rgb2hsv(col).y;

					if (abs(s2 - s) > _Threshold)
					{
						error = 1;
					}

					uv = float2(input.uv.x - i / _ScreenRes.x, input.uv.y);
					col = tex2D(_MainTex, uv);
					s2 = rgb2hsv(col).y;

					if (abs(s2 - s) > _Threshold)
					{
						error = 1;
					}

					uv = float2(input.uv.x, input.uv.y + i / _ScreenRes.y);
					col = tex2D(_MainTex, uv);
					s2 = rgb2hsv(col).y;

					if (abs(s2 - s) > _Threshold)
					{
						error = 1;
					}

					uv = float2(input.uv.x, input.uv.y - i / _ScreenRes.y);
					col = tex2D(_MainTex, uv);
					s2 = rgb2hsv(col).y;

					if (abs(s2 - s) > _Threshold)
					{
						error = 1;
					}
				}

                return fixed4((1 - s) * (1 - error) + error * _Color, 1);
            }
            ENDCG
        }
    }
}
