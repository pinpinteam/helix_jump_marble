﻿Shader "Hidden/GrayscaleDebugger"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {   
        Pass
        {
            CGPROGRAM
            #pragma vertex vert_img
            #pragma fragment frag

            #include "UnityCG.cginc"

            sampler2D _MainTex;
			float _Treshold;
			float _Debug;

            fixed4 frag (v2f_img input) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, input.uv);
                
				float lum = col.r*.3 + col.g*.59 + col.b*.11;
				
				float thresholdStep = step(_Treshold, lum) * _Debug;

				return thresholdStep * fixed4(1,0,0,1) + (1 - thresholdStep) * lum;
            }
            ENDCG
        }
    }
}
