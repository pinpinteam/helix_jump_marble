using System;
using System.Collections;
using System.Collections.Generic;
using FunGames.Sdk.Analytics;
using FunGamesSdk;
using OgurySdk;
using Pinpin.UI;
using UnityEngine;
#if UNITY_IOS && !UNITY_EDITOR
using FunGames.Sdk.Analytics;
using FunGames.Sdk.RemoteConfig;
using FunGamesSdk.FunGames.AppTrackingManager;
#endif

public class FunGamesManager : MonoBehaviour
{
	[SerializeField] GDPRPopup m_GDPRPopup;
	// Start is called before the first frame update
	void Start ()
	{
#if UNITY_IOS && !UNITY_EDITOR
        if (FunGamesAppTrackingTransparency.ShouldUseATT())
        {
            FunGamesAnalytics.NewDesignEvent("ShowingATT", "true");
            FunGamesAppTrackingTransparency._instance.RequestAuthorizationAppTrackingTransparency(FinishTracking);
        }
        else
        {
            FinishTracking();
        }
#else
		FinishTracking();
#endif
	}

	private void Update ()
	{
		var settings = FunGamesSettings.settings;
		if (settings.useMax)
		{
			FunGamesMax.Update();
		}
	}

	public void FinishTracking ()
	{
#if UNITY_IOS && !UNITY_EDITOR
        if(FunGamesAppTrackingTransparency.ShouldUseATT() && FunGamesAppTrackingTransparency._instance.isTrackingAllow())
            FunGamesAnalytics.NewDesignEvent("AllowTracking", "true");
#endif
		var settings = FunGamesSettings.settings;

		FunGamesAnalytics.Initialize();

		if (settings.useOgury)
		{
			Debug.Log("Initialize Ogury");
			new GameObject("OguryCallbacks", typeof(OguryCallbacks));

			Ogury.Start(settings.oguryAndroidAssetKey, settings.oguryIOSAssetKey);

			FunGamesThumbail.Start();
		}
		else if (settings.useMax)
		{
			Debug.Log("Initialize Max");
			FunGamesMax.Start(m_GDPRPopup);
		}
		FunGamesFB.Start();
	}
}
