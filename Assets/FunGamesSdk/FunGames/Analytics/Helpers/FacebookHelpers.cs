using UnityEngine;
using Facebook.Unity;
using FunGames.Sdk;
using FunGamesSdk.FunGames.AppTrackingManager;
using System;
using System.Collections.Generic;

namespace FunGamesSdk.FunGames.Analytics.Helpers
{
    public class FacebookHelpers
    {
		class FBEvent
		{
			public string eventName;
			public Dictionary<string, object> parameters;
		}
		private static List<FBEvent> storedEvents = new List<FBEvent>();

        public static void Initialize ()
        {
            var settings = FunGamesSettings.settings;

            if (settings.useFacebook == false)
            {
                return;
            }
            
            if (!FB.IsInitialized) {
                FB.Init(settings.facebookGameID, null, true, true, true, false,
                    true, null, "en_US", OnHideUnity, InitCallback);
            } else {
                FB.ActivateApp();
            }
        }

        private static void InitCallback ()
        {
            if (FB.IsInitialized)
            {
#if UNITY_IOS
				if (FunGamesAppTrackingTransparency.ShouldUseATT())
					FB.Mobile.SetAdvertiserTrackingEnabled(FunGamesAppTrackingTransparency._instance.isTrackingAllow());
#endif
                FB.ActivateApp();
				for (int i = 0; i < storedEvents.Count; i++)
				{
					FBEvent fbEvent = storedEvents[i];
					FB.LogAppEvent(fbEvent.eventName, null, fbEvent.parameters);
				}
				storedEvents.Clear();

			}
            else
            {
                Debug.Log("Failed to Initialize the Facebook SDK");
            }
        }
        
        private static void OnHideUnity (bool isGameShown)
        {
            if (!isGameShown) {
                // Pause the game - we will need to hide
            } else {
                // Resume the game - we're getting focus again
            }
        }

		public static void LogAppEvent ( string eventName, Dictionary<string, object> parameters = null )
		{
			if (FB.IsInitialized)
			{
				FB.LogAppEvent(eventName, null, parameters);
			}
			else
			{
				storedEvents.Add(new FBEvent { eventName = eventName, parameters = parameters });
			}
		}
	}
}