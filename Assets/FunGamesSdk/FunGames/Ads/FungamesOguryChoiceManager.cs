using FunGamesSdk;
using OgurySdk;
using UnityEngine;

public class FungamesOguryChoiceManager
{
	// Start is called on the start of FunGamesAds
	internal static void Start()
    {
		var settings = FunGamesSettings.settings;

		if (settings.useOgury)
		{
			if (!PlayerPrefs.HasKey("GDPRAnswered"))
			{
				OguryChoiceManager.OnAskComplete += OnCMComplete;
				OguryChoiceManager.OnAskError += OnCMError;
				OguryChoiceManager.Ask();
			}
			else
				StartSdks();
		}
	}
	
	private static void OnCMComplete ( OguryChoiceManager.Answer answer )
	{
		// will serve personalized ads when the user consents
		if (OguryChoiceManager.Answer.FullApproval == answer)
		{
			PlayerPrefs.SetInt("GDPRAnswered", 1);
		}
		else
		{
			PlayerPrefs.SetInt("GDPRAnswered", 0);
		}
		StartSdks();
	}
	
	private static void OnCMError ( OguryError error )
	{
		// will serve personalized ads when the user consents
		StartSdks();
	}
	
	private static void StartSdks ()
	{
		// initialize Ogury Ads
		Ogury.StartAds();

		var settings = FunGamesSettings.settings;
		if (settings.useMax)
		{
			Debug.Log("Initialize Max");
			FunGamesMax.Start();
		}
	}
}